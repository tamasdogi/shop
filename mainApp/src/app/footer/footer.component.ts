import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    /* Back To Top Button */
    // Get the button
    const myButton = document.getElementById('myBtn');

    // When the user scrolls down 20px from the top of the document, show the button
    /*
    function scrollFunctionBTT() {
      if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        myButton.style.display = 'block';
      } else {
        myButton.style.display = 'none';
      }
    }*/
  }

  // When the user clicks on the button, scroll to the top of the document
  // tslint:disable-next-line:typedef
  topFunction() {
    document.body.scrollTop = 0; // for Safari
    document.documentElement.scrollTop = 0; // for Chrome, Firefox, IE and Opera
  }

}
